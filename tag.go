// Copyright 2018 tugnuts. All rights reserved.

package phpbbxf2

import (
	"database/sql"
)

type xfTag struct {
	TagID       int
	TagTitle    string
	TagURL      string
	LastUseDate int
}

var tagTitleToTagMap = map[string]*xfTag{}
var tagsMapPreparedGuard bool

func (o *Import) prepareTags() error {
	if tagsMapPreparedGuard {
		return nil
	}

	var (
		tagRows     *sql.Rows
		tagID       int
		tagTitle    string
		tagURL      string
		lastUseDate int
	)

	tagRows, err = xf2.Txn.Query(`
        SELECT tag_id, tag, tag_url, last_use_date
        FROM xf_tag
    `)
	if err != nil {
		return err
	}

	for tagRows.Next() {
		err = tagRows.Scan(&tagID, &tagTitle, &tagURL, &lastUseDate)
		if err != nil {
			return err
		}

		_, ok := tagTitleToTagMap[tagURL]
		if !ok {
			tagTitleToTagMap[tagURL] = &xfTag{
				TagID:       tagID,
				TagTitle:    tagTitle,
				TagURL:      tagURL,
				LastUseDate: lastUseDate,
			}
		}
	}

	tagsMapPreparedGuard = true

	return err
}

func (o *Import) insertTag(t *xfTag) (int, error) {
	var res sql.Result
	var lastInsertID int64

	res, err = xf2.Txn.Exec(`
        INSERT INTO xf_tag
        SET tag = ?,
            tag_url = ?,
            last_use_date = ?
    `, t.TagTitle, t.TagURL, t.LastUseDate)
	if err != nil {
		return 0, err
	}

	lastInsertID, err = res.LastInsertId()
	if err != nil {
		return 0, err
	}

	lastInsertIDInt := int(lastInsertID)

	t.TagID = lastInsertIDInt
	tagTitleToTagMap[t.TagURL] = t

	return lastInsertIDInt, err
}
