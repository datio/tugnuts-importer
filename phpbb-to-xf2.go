// Copyright 2018 tugnuts. All rights reserved.

package phpbbxf2

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql" // The MySQL-MariaDB driver.
	toml "github.com/pelletier/go-toml"
)

// The Import options are populated in main() via the CLI.
type Import struct {
	Config *toml.Tree
}

type xf2Installation struct {
	Db  *sql.DB
	Txn *sql.Tx

	XfDir string
}

type phpbbInstallation struct {
	Db  *sql.DB
	Txn *sql.Tx

	PhpbbDataDir string
	G2DataDir    string
}

var (
	err error
	xf2 *xf2Installation

	phpbb *phpbbInstallation

	currentTime      = time.Now()
	currentTimeEpoch = currentTime.Unix()
)

// Begin populates the installation related variables and initiates the database connections.
func (o *Import) Begin() error {
	xf2 = &xf2Installation{}

	xf2Dsn := fmt.Sprintf(
		"%s:%s@(%s:%d)/%s?autocommit=true&charset=utf8mb4,utf8&collation=utf8mb4_unicode_ci&parseTime=true&timeout=48h",
		o.Config.Get("XenForo2.db_user").(string),
		o.Config.Get("XenForo2.db_password").(string),
		o.Config.Get("XenForo2.db_host").(string),
		o.Config.Get("XenForo2.db_port").(int64),
		o.Config.Get("XenForo2.db_name").(string),
	)

	xf2.Db, err = sql.Open("mysql", xf2Dsn)
	if err != nil {
		return AddErrorInfo(err, "xenforo2 db")
	}

	xf2.Db.SetMaxIdleConns(0)
	xf2.Db.SetConnMaxLifetime(48 * time.Hour)

	err = xf2.Db.Ping()
	if err != nil {
		return AddErrorInfo(err, "xenforo2 db")
	}

	xf2.Txn, err = xf2.Db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		_ = xf2.Txn.Rollback()
		_ = xf2.Db.Close()
	}()

	phpbb = &phpbbInstallation{}

	phpbbDsn := fmt.Sprintf(
		"%s:%s@(%s:%d)/%s?parseTime=false&timeout=48h",
		o.Config.Get("PhpBB.db_user").(string),
		o.Config.Get("PhpBB.db_password").(string),
		o.Config.Get("PhpBB.db_host").(string),
		o.Config.Get("PhpBB.db_port").(int64),
		o.Config.Get("PhpBB.db_name").(string),
	)

	phpbb.Db, err = sql.Open("mysql", phpbbDsn)
	if err != nil {
		return AddErrorInfo(err, "phpbb db")
	}

	phpbb.Db.SetMaxIdleConns(0)
	phpbb.Db.SetConnMaxLifetime(48 * time.Hour)

	err = phpbb.Db.Ping()
	if err != nil {
		return AddErrorInfo(err, "phpbb db")
	}

	readOnly := &sql.TxOptions{ReadOnly: true}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	phpbb.Txn, err = phpbb.Db.BeginTx(ctx, readOnly)
	if err != nil {
		return err
	}

	return o.migrate()
}

// AddErrorInfo concats an error value with a custom message, forming a new error.
// The message string is appended (in parentheses) at the start of the result.
func AddErrorInfo(err error, msg string) error {
	if len(msg) > 0 {
		return fmt.Errorf("(%s) %s", msg, err.Error())
	}
	return err
}
