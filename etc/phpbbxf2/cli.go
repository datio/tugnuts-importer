// Copyright 2018 tugnuts. All rights reserved.

package main

import (
	"flag"
	"fmt"
	"os"

	toml "github.com/pelletier/go-toml"
	phpbbxf2 "gitlab.com/datio/tugnuts-importer"
)

type cli struct {
	ConfigFile string
}

var (
	importOptions *phpbbxf2.Import
	cliOptions    *cli
	config        *toml.Tree
	err           error
)

func init() {
	importOptions = &phpbbxf2.Import{}
	cliOptions = &cli{}

	flag.StringVar(&cliOptions.ConfigFile, "config", "", "configuration file")

	// Parse the above CLI arguments.
	flag.Parse()

	// Initialize the configuration.
	config, err = toml.LoadFile(cliOptions.ConfigFile)
	if err != nil {
		fmt.Println(phpbbxf2.AddErrorInfo(err, "configuration error"))
		os.Exit(1)
	}
}

func main() {
	importOptions.Config = config
	err = importOptions.Begin()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
