<?php

namespace Datio\PhpBb\Pub\Controller;

use XF\Pub\Controller\AbstractController;
use XF\Mvc\ParameterBag;

class Gallery2 extends AbstractController
{
    public function actionIndex(ParameterBag $params)
    {
        $itemId = $this->filter('g2_itemId', 'uint');

        if (!$itemId) {
            return $this->redirectPermanently(
                $this->buildLink('canonical:media/index')
            );
        }

        if ($params->get('path') == 'main') {
            return $this->redirectToMediaItemFile($itemId);
        }

        return $this->redirectToMediaViewEdit($itemId);
    }

    protected function redirectToMediaItemFile($itemId)
    {
        /** @var \XFMG\Entity\MediaItem $media */
        $media = $this->app->find('XFMG:MediaItem', $itemId);

        if (!$media && $itemId != 0) {
			/** @var \XF\Mvc\Entity\Finder $mediaFinder */
			$mediaFinder = $this->app->finder('XFMG:MediaItem')
				->where('media_id', '<=', $itemId)
				->order('media_id', 'DESC');
			/** @var \XFMG\Entity\MediaItem $media */
			$media = $mediaFinder->fetchOne();

            if (!$media) {
                return $this->error(\XF::phrase('requested_media_item_not_found'), 404);
            }

            // Return the thumbnail.
            return $this->app()->response()->redirect($media->getThumbnailUrl(true), 302)->send();
        }

        // Return the media file.
        return $this->redirectPermanently(
            $this->buildLink('canonical:media/full', $media)
        );
    }

    protected function redirectToMediaViewEdit($itemId)
    {
        /** @var \Datio\PhpBb\Finder\GalleryItemType $itemTypeFinder */
        $itemTypeFinder = $this->app->finder('Datio\PhpBb:GalleryItemType')
            ->where('item_id', $itemId);
        /** @var \Datio\PhpBb\Entity\GalleryItemType $galleryItemType */
        $galleryItemType = $itemTypeFinder->fetchOne();

        if (!$galleryItemType) {
            $media = $this->app->find('XFMG:MediaItem', $itemId);

            if ($media) {
                return $this->redirectPermanently(
                    $this->buildLink('canonical:media', $media)
                );
            }
        }

        $itemType = isset($galleryItemType->item_type) ? $galleryItemType->item_type : '';

        switch ($itemType) {
            case 'album':
            case 'container':
                $category = $this->app->find('XFMG:Category', $itemId);

                return $this->redirectPermanently(
                    $this->buildLink('media/categories', $category)
                );
                break;
            default:
                return $this->redirectPermanently(
                    $this->buildLink('canonical:media/index')
                );
        }
    }
}
