<?php

namespace Datio\PhpBb\Entity;

use XF\Mvc\Entity\Structure;

/**
 * COLUMNS
 *
 * @property int    item_id
 * @property string item_type
 */
class GalleryItemType extends \XF\Mvc\Entity\Entity
{
	public static function getStructure(Structure $structure)
	{
		$structure->table      = 'xf_datio_tugnuts_mg_type';
		$structure->shortName  = 'Datio\React:GalleryItemType';
		$structure->primaryKey = 'item_id';
		$structure->columns    = [
			'item_id'   => ['type' => self::UINT],
			'item_type' => ['type' => self::STR, 'default' => 'media', ['media', 'album', 'container']],
		];

		return $structure;
	}
}