<?php

namespace Datio\PhpBb\XF\Entity;

class Route extends XFCP_Route
{
	public static function getStructure(\XF\Mvc\Entity\Structure $structure)
	{
		$structure = parent::getStructure($structure);

		if ( ! isset($structure->columns['route_prefix']['match'][0])) {
			return $structure;
		}

		$routePrefixValidationRe = $structure->columns['route_prefix']['match'][0];
		$pregDelimiter           = substr($routePrefixValidationRe, 0, 1);

		$pattern = '#(' . preg_quote($pregDelimiter, '#') . ')(.*?)(' .
			preg_quote($pregDelimiter, '#') . '.*)#';

		$replacement = '${1}(?:${2})|^gallery2' . preg_quote('.', $pregDelimiter) . 'php${3}';

		$structure->columns['route_prefix']['match'][0] = preg_replace($pattern, $replacement,
			$routePrefixValidationRe);

		return $structure;
	}
}

// ******************** FOR IDE AUTO COMPLETE ********************
if (false) {
	class XFCP_Route extends \XF\Entity\Route
	{
	}
}