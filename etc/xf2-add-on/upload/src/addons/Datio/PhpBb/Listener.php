<?php

namespace Datio\PhpBb;

class Listener
{
	public static function dispacherMatch(\XF\Mvc\Dispatcher $dispatcher, \XF\Mvc\RouteMatch &$match)
	{
		if ($dispatcher->getRequest()->getRoutePath() == 'gallery2.php') {
			$match = $dispatcher->route('gallery2.php/');
		}
	}
}