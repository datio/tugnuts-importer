<?php

namespace Datio\PhpBb\Job;

class ConvertCategoriesToAlbums extends \XF\Job\AbstractJob
{
	protected $defaultData = [
		'convertCategoryId' => 7001,
	];

	protected $affectedMediaIds = [];

	public function run($maxRunTime)
	{
		$app = \XF::app();
		$db = $app->db();

		$db->beginTransaction();

		$db->query("
			UPDATE xf_mg_category child, xf_mg_category parent
			SET child.title = parent.title
			WHERE parent.category_id = child.parent_category_id
			  AND child.title = 'Other';
		");

		$childCategoryIdToParentCategoryIdMap = [];
		$categoryIds = [];

		$categoriesMap = $db->fetchAll("
			SELECT category_id, parent_category_id
			FROM xf_mg_category
			ORDER BY parent_category_id
		");

		foreach ($categoriesMap as $categoryMap) {
			$childCategoryId = $categoryMap['category_id'];
			$parentCategoryId = $categoryMap['parent_category_id'];

			if ($parentCategoryId == 0 && $childCategoryId != $this->defaultData['convertCategoryId']) {
				continue;
			}

			if ($parentCategoryId != 0 && !isset($childCategoryIdToParentCategoryIdMap[$parentCategoryId])) {
				continue;
			}

			$childCategoryIdToParentCategoryIdMap[$childCategoryId] = $parentCategoryId;

			array_push($categoryIds, $childCategoryId);
		}

		$categoryFinder = $app->finder('XFMG:Category');

		/** @var \XFMG\Entity\Category[] $categories */
		$categories = $categoryFinder
			->where('category_id', $categoryIds)
			->fetch();

		if ($categoryFinder->total() == 0) {
			return $this->complete();
		}

		foreach ($categories as $category) {
			switch ($category->category_type) {
				case 'media':
					$this->convertMediaCategory($category);
					break;
				case 'container':
					break;
				case 'album':
					break;
			}
		}

		foreach ($categories as $category) {
			$category->delete(false, false);
		}

		$db->query("
			UPDATE xf_mg_media_item
			SET album_id = category_id,
			    category_id = 0
			WHERE category_id IN (" . $db->quote($categoryIds) . ")
		");

		$db->commit();

		return $this->complete();
	}

	protected function convertMediaCategory(\XFMG\Entity\Category $category)
	{
		$app = \XF::app();
		/** @var \XFMG\Entity\Album $album */
		$album = $app->em()->create('XFMG:Album');

		/** @var \XFMG\Entity\MediaItem $firstMediaInAlbum */
		$firstMediaInAlbum = $app->finder('XFMG:MediaItem')
			->where('category_id', $category->category_id)
			->order('media_date')
			->fetchOne();

		if (!$firstMediaInAlbum) {
			return;
		}

		/** @var \XF\Entity\User $user */
		$user = $app->finder('XF:User')
			->where('user_id', $firstMediaInAlbum->user_id)
			->fetchOne();

		$album->set('album_id', $category->category_id, ['forceSet' => true]);
		$album->category_id = 0; // $this->defaultData['convertedAlbumsContainerCategoryId'];
		$album->title = $category->title;
		$album->description = $category->description;
		$album->create_date = $firstMediaInAlbum->media_date;
		$album->media_item_cache = [];
		$album->view_privacy = 'public';
		$album->view_users = [];
		$album->add_users = [];
		$album->user_id = $firstMediaInAlbum->user_id;
		$album->username = $firstMediaInAlbum->username;

		$album->save(true, false);
	}

	public function getStatusMessage()
	{
		$actionPhrase = \XF::phrase('converting');
		$typePhrase = \XF::phrase('categories');
		return sprintf('%s... %s', $actionPhrase, $typePhrase);
	}

	public function canCancel()
	{
		return false;
	}

	public function canTriggerByChoice()
	{
		return true;
	}
}