<?php

namespace Datio\PhpBb;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;

class Setup extends AbstractSetup
{
	public function postInstall(array &$stateChanges)
	{
		$this->postUpgrade(0, $stateChanges);
	}

	public function postUpgrade($previousVersion, array &$stateChanges)
	{
		if ($previousVersion <= 1000010) {
			$this->query("
			INSERT IGNORE INTO xf_route
			SET route_type = ?,
			    route_prefix = ?,
			    sub_name = '',
			    format = ?,
			    build_class = '',
			    build_method = '',
			    controller = ?,
			    context = '',
			    action_prefix = '',
			    addon_id = ''
			", [
					'public',
					'gallery2.php',
					':any<path>',
					'Datio\PhpBb:Gallery2',
				]
			);

			$this->app->em()->getRepository('XF:Route')->rebuildRouteCaches();
		}
	}

	use StepRunnerInstallTrait;
	use StepRunnerUpgradeTrait;
	use StepRunnerUninstallTrait;

	public function installStep1()
	{
	}

	public function upgrade1000016Step1()
	{
	}

	public function uninstallStep1()
	{
		$this->query("
			DELETE FROM xf_route
			WHERE route_type = ?
			  AND route_prefix = ?
			  AND sub_name = ''
		", [
				'public',
				'gallery2.php',
			]
		);
	}
}