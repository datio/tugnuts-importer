// Copyright 2018 tugnuts. All rights reserved.

package phpbbxf2

import (
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"html"
	"io"
	"math"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"github.com/Machiel/slugify"
	"github.com/oliamb/cutter"
	"github.com/rkravchik/resize"

	"image"
	"image/gif"
	"image/jpeg"
	"image/png"

	_ "golang.org/x/image/bmp"  // Understand BMPs.
	_ "golang.org/x/image/tiff" // Understand TIFFs.
)

func (o *Import) createXFMGImportLogTable() error {
	_, err = xf2.Txn.Exec(`
		CREATE TABLE IF NOT EXISTS xf_datio_tugnuts_mg_type (
			item_id INT NOT NULL,
			item_type enum('media','album','container') NOT NULL DEFAULT 'media',
			PRIMARY KEY (item_id)
		)
	`)

	return err
}

type phpbbGalleryItem struct {
	ID                   int
	CanContainChildren   bool
	Description          string
	Keywords             string // Used as tags. Multiple values are separated with a semicolon.
	OwnerUserID          int
	Summary              string // Use this as the description if the description field itself is empty.
	Title                string // Can be empty, use filename as title in that case.
	PathComponent        string
	ParentID             int
	OriginationTimestamp int

	ContainerAlternativeAlbumID int // XFMG media items can't have albums as simblings. They will be included in new albums, named "Other", as a workaround.
	Tags                        []*xfTag

	FilePath string
}

type itemProperties struct {
	ContainsAlbums bool
	ContainsImages bool
	IsAlbum        bool
	IsRoot         bool
}

func (o *Import) importMediaGallery() error {
	doImport, ok := o.Config.Get("phpbbxf2.import_media").(bool)
	if !ok {
		return errors.New("could not find value of 'phpbbxf2.import_media' in config")
	}

	if !doImport {
		return nil
	}

	var items = []*phpbbGalleryItem{}
	var itemIDToItemMap = map[int]*phpbbGalleryItem{}

	var itemsPropertiesMap = map[int]*itemProperties{}

	items, itemIDToItemMap, err = o.getAllItems(itemsPropertiesMap)
	if err != nil {
		return err
	}

	err = o.createCategoriesAndAlbums(items, itemsPropertiesMap)
	if err != nil {
		return err
	}

	err = o.assignTags(items)
	if err != nil {
		return err
	}

	err = o.prepareMedia(items, itemIDToItemMap)
	if err != nil {
		return err
	}

	err = o.createMedia(items, itemIDToItemMap)
	if err != nil {
		return err
	}

	return err
}

var rootItemID int
var lastItemID int

func (o *Import) insertMediaGalleryCategory(item *phpbbGalleryItem, itemsPropertiesMap map[int]*itemProperties) error {
	if item.ParentID == rootItemID {
		item.ParentID = 0
	}

	var categoryType string
	var allowedTypes string
	// If a g2 category item contains both image and album types, it'll' be imported as a container.
	if itemsPropertiesMap[item.ID].ContainsAlbums && itemsPropertiesMap[item.ID].ContainsImages {
		categoryType = "container"
		allowedTypes = "a:0:{}"
	} else if itemsPropertiesMap[item.ID].ContainsAlbums {
		categoryType = "album"
		allowedTypes = `a:4:{i:0;s:5:"image";i:1;s:5:"video";i:2;s:5:"audio";i:3;s:5:"embed";}`
	} else {
		categoryType = "media"
		allowedTypes = `a:4:{i:0;s:5:"image";i:1;s:5:"video";i:2;s:5:"audio";i:3;s:5:"embed";}`
	}

	breadcrumbData := fmt.Sprintf(`a:1:{i:%d;a:4:{s:11:"category_id";i:%d;s:5:"title";s:%d:"%s";s:5:"depth";i:0;s:3:"lft";i:0;}}`, item.ID, item.ID, len(item.Title), item.Title)

	_, err = xf2.Txn.Exec(`
		INSERT INTO xf_mg_category
		SET category_id = ?,
			title = ?,
			description = ?,
			parent_category_id = ?,
			breadcrumb_data = ?,
			category_type = ?,
			allowed_types = ?,
			field_cache = 'a:0:{}'
		ON DUPLICATE KEY
		UPDATE title = ?,
			   description = ?,
			   parent_category_id = ?,
			   breadcrumb_data = ?,
			   category_type = ?,
			   allowed_types = ?,
			   field_cache = 'a:0:{}'
	`,
		item.ID, item.Title, item.Description, item.ParentID, breadcrumbData, categoryType, allowedTypes,
		item.Title, item.Description, item.ParentID, breadcrumbData, categoryType, allowedTypes,
	)

	if err != nil {
		err = AddErrorInfo(err, "insert into xf_mg_category")
	}

	itemType := "album"
	if categoryType == "container" {
		itemType = "container"

		lastItemID++
		item.ContainerAlternativeAlbumID = lastItemID

		breadcrumbData = fmt.Sprintf(`a:1:{i:%d;a:4:{s:11:"category_id";i:%d;s:5:"title";s:%d:"%s";s:5:"depth";i:0;s:3:"lft";i:0;}}`, item.ContainerAlternativeAlbumID, item.ContainerAlternativeAlbumID, len("Other"), "Other")

		_, err = xf2.Txn.Exec(`
			INSERT INTO xf_mg_category
			SET category_id = ?,
				title = ?,
				description = ?,
				parent_category_id = ?,
				breadcrumb_data = ?,
				category_type = ?,
				allowed_types = ?,
				field_cache = 'a:0:{}'
			ON DUPLICATE KEY
			UPDATE title = ?,
				   description = ?,
				   parent_category_id = ?,
				   breadcrumb_data = ?,
				   category_type = ?,
				   allowed_types = ?,
				   field_cache = 'a:0:{}'
		`,
			item.ContainerAlternativeAlbumID, "Other", "", item.ID, breadcrumbData, "media", `a:4:{i:0;s:5:"image";i:1;s:5:"video";i:2;s:5:"audio";i:3;s:5:"embed";}`,
			"Other", "", item.ID, breadcrumbData, "media", `a:4:{i:0;s:5:"image";i:1;s:5:"video";i:2;s:5:"audio";i:3;s:5:"embed";}`,
		)
	}

	// Upsert import log table.
	_, err = xf2.Txn.Exec(`
		INSERT INTO xf_datio_tugnuts_mg_type
		SET item_id = ?,
			item_type = ?
		ON DUPLICATE KEY
		UPDATE item_type = ?
	`,
		item.ID, itemType,
		itemType,
	)
	if err != nil {
		return AddErrorInfo(err, "insert into xf_datio_tugnuts_mg_type")
	}

	return err
}

func (o *Import) getAllItems(itemsPropertiesMap map[int]*itemProperties) ([]*phpbbGalleryItem, map[int]*phpbbGalleryItem, error) {
	var items = []*phpbbGalleryItem{}
	var itemIDToItemMap = map[int]*phpbbGalleryItem{}

	var (
		itemRows             *sql.Rows
		id                   int
		canContainChildren   bool
		description          string
		keywords             string
		ownerUserID          int
		summary              string
		title                string
		pathComponent        string
		parentID             int
		originationTimestamp int
	)

	itemRows, err = phpbb.Txn.Query(`
		SELECT g2_Item.g_id,
			   g2_Item.g_canContainChildren,
			   COALESCE(g2_Item.g_description, '') AS g_description,
			   COALESCE(g2_Item.g_keywords, '') AS g_keywords,
			   g2_Item.g_ownerId,
			   COALESCE(g2_Item.g_summary, '') AS g_summary,
			   COALESCE(g2_Item.g_title, '') AS g_title,
			   COALESCE(g2_FileSystemEntity.g_pathComponent, '') AS g_pathComponent,
			   g2_ChildEntity.g_parentId,
			   g2_Item.g_originationTimestamp
		FROM g2_Item
		LEFT JOIN g2_FileSystemEntity ON g2_FileSystemEntity.g_id = g2_Item.g_id
		LEFT JOIN g2_ChildEntity ON g2_ChildEntity.g_id = g2_Item.g_id
		# WHERE g2_Item.g_id = 15782 OR g2_Item.g_id = 6999 OR g2_Item.g_id = 7 OR g2_Item.g_id = 7762 OR g2_Item.g_id = 7769
		ORDER BY g2_ChildEntity.g_parentId
	`)
	if err != nil {
		return nil, nil, err
	}

	for itemRows.Next() {
		err = itemRows.Scan(&id, &canContainChildren, &description, &keywords, &ownerUserID, &summary, &title, &pathComponent, &parentID, &originationTimestamp)
		if err != nil {
			return nil, nil, AddErrorInfo(err, "select from g2_Item")
		}

		if id > lastItemID {
			lastItemID = id
		}

		if len(title) == 0 {
			title = pathComponent
		}

		items = append(items, &phpbbGalleryItem{
			ID:                   id,
			CanContainChildren:   canContainChildren,
			Description:          html.UnescapeString(description),
			Keywords:             keywords,
			OwnerUserID:          ownerUserID,
			Summary:              html.UnescapeString(summary),
			Title:                html.UnescapeString(title),
			PathComponent:        pathComponent,
			ParentID:             parentID,
			OriginationTimestamp: originationTimestamp,
		})

		itemIDToItemMap[id] = items[len(items)-1]

		_, ok := itemsPropertiesMap[id]
		if !ok {
			itemsPropertiesMap[id] = &itemProperties{}
		}
		_, ok = itemsPropertiesMap[parentID]
		if !ok {
			itemsPropertiesMap[parentID] = &itemProperties{}
		}

		if canContainChildren {
			itemsPropertiesMap[id].IsAlbum = true
			itemsPropertiesMap[parentID].ContainsAlbums = true
		} else {
			itemsPropertiesMap[parentID].ContainsImages = true
		}

		if parentID == 0 && rootItemID == 0 {
			rootItemID = id
			itemsPropertiesMap[id].IsRoot = true
			continue
		}
	}

	return items, itemIDToItemMap, err
}

func (o *Import) createCategoriesAndAlbums(items []*phpbbGalleryItem, itemsPropertiesMap map[int]*itemProperties) error {
	for _, item := range items {
		if itemsPropertiesMap[item.ID].IsRoot {
			continue
		}

		if itemsPropertiesMap[item.ID].IsAlbum {
			err = o.insertMediaGalleryCategory(item, itemsPropertiesMap)
			if err != nil {
				return err
			}
		}
	}

	return err
}

func (o *Import) assignTags(items []*phpbbGalleryItem) error {
	// recursively assign parent tags onto media
	err = o.prepareTags()
	if err != nil {
		return err
	}

	for _, item := range items {
		if len(item.Keywords) == 0 {
			continue
		}

		separator := ";"
		if strings.Contains(item.Keywords, ",") {
			separator = ","
		}

		tags := strings.Split(item.Keywords, separator)
		for _, tagTitle := range tags {
			tagTitle = strings.TrimSpace(html.UnescapeString(tagTitle))
			if len(tagTitle) > 100 {
				continue
			}

			slugifiedTagTitle := slugify.Slugify(tagTitle)

			if len(slugifiedTagTitle) == 0 || len(slugifiedTagTitle) > 18 {
				continue
			}

			tag, ok := tagTitleToTagMap[slugifiedTagTitle]
			if !ok {
				_, err = o.insertTag(&xfTag{
					TagTitle:    tagTitle,
					TagURL:      slugifiedTagTitle,
					LastUseDate: int(currentTimeEpoch),
				})
				if err != nil {
					return err
				}

				tag = tagTitleToTagMap[slugifiedTagTitle]
			}

			item.Tags = append(item.Tags, tag)
		}

	}

	return err
}

// Recursively build the path and tag collections based on the item parents.
func (o *Import) prepareMedia(items []*phpbbGalleryItem, itemIDToItemMap map[int]*phpbbGalleryItem) error {
	g2DataDir, ok := o.Config.Get("PhpBB.g2data_dir").(string)
	if !ok {
		return errors.New("could not find value of 'PhpBB.g2data_dir' in config")
	}

	for _, item := range items {
		if item.CanContainChildren {
			continue
		}

		var path string
		var tagURLs = []string{}

		imageItem := item
		currentItem := item
		for {
			path = "/" + currentItem.PathComponent + path

			for j := 0; j <= len(currentItem.Tags)-1; j++ {
				var tagInList bool

				for i := 0; i <= len(tagURLs)-1; i++ {
					if tagURLs[i] == currentItem.Tags[j].TagURL {
						tagInList = true
						break
					}
				}

				if !tagInList {
					tagURLs = append(tagURLs, currentItem.Tags[j].TagURL)
				}
			}

			sort.Strings(tagURLs)

			imageItem.Tags = []*xfTag{}
			for _, tagURL := range tagURLs {
				imageItem.Tags = append(imageItem.Tags, tagTitleToTagMap[tagURL])
			}

			if currentItem.ParentID == 0 {
				break
			}

			currentItem = itemIDToItemMap[currentItem.ParentID]
		}

		item.FilePath = fmt.Sprintf("%s/albums/", g2DataDir) + strings.TrimLeft(path, "/")

		// Check
	}

	return err
}

var thumbnailOptionRe = regexp.MustCompile(`"width":([0-9]+).*"height":([0-9]+)`)

func (o *Import) createMedia(items []*phpbbGalleryItem, itemIDToItemMap map[int]*phpbbGalleryItem) error {
	xf2RootDir, ok := o.Config.Get("XenForo2.xenforo_directory").(string)
	if !ok {
		return errors.New("could not find value of 'XenForo2.xenforo_directory' in config")
	}

	var counter int

	for _, item := range items {
		if item.CanContainChildren {
			continue
		}

		if counter >= 999 {
			err = xf2.Txn.Commit()
			if err != nil {
				return err
			}
			xf2.Txn, err = xf2.Db.Begin()
			if err != nil {
				return err
			}

			counter = 0
		}

		var imageFile *os.File
		imageFile, err = os.OpenFile(item.FilePath, os.O_RDONLY, 0644)
		if err != nil {
			fmt.Println(AddErrorInfo(err, fmt.Sprintf("item id: %d filename: %s", item.ID, item.PathComponent)).Error())

			_ = imageFile.Close()
			continue
		}

		_, err = imageFile.Seek(0, 0)
		if err != nil {
			return err
		}

		var attachmentImage image.Image
		var format string

		attachmentImage, format, err = image.Decode(imageFile)
		if err != nil {
			if strings.HasSuffix(strings.ToLower(item.FilePath), ".pdf") {
				// todo: Convert pdf files to images.
			}

			fmt.Println(AddErrorInfo(err, fmt.Sprintf("item id: %d filename: %s", item.ID, item.PathComponent)).Error())

			_ = imageFile.Close()
			continue
		}

		var attachmentImageGIF *gif.GIF
		if format == "gif" {
			_, err = imageFile.Seek(0, 0)
			if err != nil {
				return err
			}

			attachmentImageGIF, err = gif.DecodeAll(imageFile)
		}

		_ = imageFile.Close()

		subDir := int(math.Floor(float64(item.ID) / 1000))
		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sinternal_data/attachments/%d/", xf2RootDir, subDir), true)
		if err != nil {
			return err
		}

		tempPath := fmt.Sprintf("%sinternal_data/attachments/%d/%d_temp.data", xf2RootDir, subDir, item.ID)
		imageFile, err = os.Create(tempPath)
		if err != nil {
			return err
		}

		if format == "gif" {
			err = gif.EncodeAll(imageFile, attachmentImageGIF)
			if err != nil {
				err = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
			}
		} else {
			err = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
		}
		if err != nil {
			return err
		}

		// Calculate file hash.
		_, err = imageFile.Seek(0, 0)
		if err != nil {
			return err
		}
		h := md5.New()
		if _, err = io.Copy(h, imageFile); err != nil {
			_ = imageFile.Close()
			return err
		}
		mediaFileHash := fmt.Sprintf("%x", h.Sum(nil))

		var imageFileInfo os.FileInfo
		imageFileInfo, err = imageFile.Stat()
		imageFileSize := imageFileInfo.Size()

		_ = imageFile.Close()

		properPath := fmt.Sprintf("%sinternal_data/attachments/%d/%d-%s.data", xf2RootDir, subDir, item.ID, mediaFileHash)
		err = os.Rename(tempPath, properPath)
		if err != nil {
			return err
		}

		// Create media thumbnails.
		var serializedOptionValue string
		var thumbnailWidth, thumbnailHeight int
		err = xf2.Txn.QueryRow(`
			SELECT option_value FROM xf_option
			WHERE option_id = 'xfmgThumbnailDimensions'
		`).Scan(&serializedOptionValue)
		if err != nil {
			return err
		}

		optionItemsMatches := thumbnailOptionRe.FindStringSubmatch(serializedOptionValue)
		thumbnailWidth, err = strconv.Atoi(optionItemsMatches[1])
		if err != nil {
			return AddErrorInfo(err, "thumbnailWidth")
		}

		thumbnailHeight, err = strconv.Atoi(optionItemsMatches[2])
		if err != nil {
			return AddErrorInfo(err, "thumbnailHeight")
		}

		if thumbnailHeight == 0 || thumbnailWidth == 0 {
			thumbnailHeight = 300
			thumbnailWidth = 300
		}

		b := attachmentImage.Bounds()
		originalImageWidth := b.Max.X
		originalImageHeight := b.Max.Y

		if b.Max.X >= b.Max.Y {
			if float64(b.Max.Y)/float64(b.Max.X) < 0.001 {
				fmt.Println(AddErrorInfo(err, fmt.Sprintf("item id: %d filename: %s", item.ID, item.PathComponent)).Error())
				continue
			}
			attachmentImage = resize.Resize(0, uint(thumbnailHeight), attachmentImage, resize.Lanczos3)
		} else {
			if float64(b.Max.X)/float64(b.Max.Y) < 0.001 {
				fmt.Println(AddErrorInfo(err, fmt.Sprintf("item id: %d filename: %s", item.ID, item.PathComponent)).Error())
				continue
			}
			attachmentImage = resize.Resize(uint(thumbnailWidth), 0, attachmentImage, resize.Lanczos3)
		}

		attachmentImage, err = cutter.Crop(attachmentImage, cutter.Config{
			Width:  thumbnailWidth,
			Height: thumbnailHeight,
			Mode:   cutter.Centered,
		})
		if err != nil {
			return err
		}

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/xfmg/thumbnail/%d/", xf2RootDir, subDir), true)
		if err != nil {
			return err
		}

		imageFile, err = os.Create(fmt.Sprintf("%sdata/xfmg/thumbnail/%d/%d-%s.jpg", xf2RootDir, subDir, item.ID, mediaFileHash))
		if err != nil {
			return err
		}
		b = attachmentImage.Bounds()

		newThumbnailWidth := b.Max.X
		newThumbnailHeight := b.Max.Y

		_ = jpeg.Encode(imageFile, attachmentImage, &jpeg.Options{Quality: 100})
		_ = imageFile.Close()

		err = o.createEmptyDirIfNotExists(fmt.Sprintf("%sdata/attachments/%d/", xf2RootDir, subDir), true)
		if err != nil {
			return err
		}

		imageFile, err = os.Create(fmt.Sprintf("%sdata/attachments/%d/%d-%s.jpg", xf2RootDir, subDir, item.ID, mediaFileHash))
		if err != nil {
			return err
		}

		_ = png.Encode(imageFile, attachmentImage)
		_ = imageFile.Close()

		for _, t := range item.Tags {
			_, err = xf2.Txn.Exec(`
				INSERT IGNORE INTO xf_tag_content
				SET content_type = 'xfmg_media',
					content_id = ?,
					tag_id = ?,
					add_user_id = ?,
					add_date = ?,
					visible = 1,
					content_date = ?
				ON DUPLICATE KEY
				UPDATE content_date = ?
			`,
				item.ID, t.TagID, item.OwnerUserID, item.OriginationTimestamp, item.OriginationTimestamp,
				item.OriginationTimestamp,
			)
			if err != nil {
				return err
			}
		}

		album := itemIDToItemMap[item.ParentID]
		albumID := album.ID
		if album.ContainerAlternativeAlbumID != 0 && album.ID != album.ContainerAlternativeAlbumID {
			albumID = album.ContainerAlternativeAlbumID
		}

		username, ok := galleryUserIDToUsernameMap[item.OwnerUserID]
		if !ok {
			username = "N/A"
		}

		forumUserID, _ := galleryUserIDToForumID[item.OwnerUserID]

		tagsSerialized := fmt.Sprintf("a:%d:{", len(item.Tags))
		for _, t := range item.Tags {
			tagsSerialized += fmt.Sprintf(`i:%d;a:2:{s:3:"tag";s:%d:"%s";s:7:"tag_url";s:%d:"%s";}`,
				t.TagID, len(t.TagTitle), t.TagTitle, len(t.TagURL), t.TagURL)
		}
		tagsSerialized += "}"

		_, err = xf2.Txn.Exec(`
			INSERT INTO xf_mg_media_item
			SET media_id = ?,
			    media_hash = ?,
			    title = ?,
			    description = ?,
			    media_date = ?,
			    media_type = 'image',
			    media_state = ?,
			    album_id = 0,
			    category_id = ?,
			    user_id = ?,
			    username = ?,
			    custom_fields = 'a:0:{}',
			    exif_data = '[]',
			    thumbnail_date = ?,
			    tags = ?
			ON DUPLICATE KEY
			UPDATE media_hash = ?,
			       title = ?,
			       description = ?,
			       media_date = ?,
			       media_type = 'image',
			       media_state = ?,
			       album_id = 0,
			       category_id = ?,
			       user_id = ?,
			       username = ?,
			       custom_fields = 'a:0:{}',
			       exif_data = '[]',
			       thumbnail_date = ?,
			       tags = ?
		`,
			item.ID, mediaFileHash, item.Title, item.Description, item.OriginationTimestamp, "visible", albumID, forumUserID, username, item.OriginationTimestamp,
			tagsSerialized,
			mediaFileHash, item.Title, item.Description, item.OriginationTimestamp, "visible", albumID, forumUserID, username, item.OriginationTimestamp,
			tagsSerialized,
		)

		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_mg_media_watch
            SET user_id = ?,
                media_id = ?,
                notify_on = 'comment',
                send_alert = 1,
                send_email = 0
        `,
			item.OwnerUserID, item.ID,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_mg_media_watch")
		}

		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_attachment
            SET attachment_id = ?,
                data_id = ?,
                content_type = 'xfmg_media',
                content_id = ?,
                attach_date = ?,
                unassociated = 0
            ON DUPLICATE KEY
            UPDATE data_id = ?,
                   content_type = 'xfmg_media',
                   content_id = ?,
                   attach_date = ?,
                   unassociated = 0
        `,
			item.ID, item.ID, item.ID, item.OriginationTimestamp,
			item.ID, item.ID, item.OriginationTimestamp,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_attachment")
		}

		_, err = xf2.Txn.Exec(`
            INSERT IGNORE INTO xf_attachment_data
            SET data_id = ?,
                user_id = ?,
                upload_date = ?,
                filename = ?,
                file_size = ?,
                file_hash = ?,
                width = ?,
                height = ?,
                thumbnail_width = ?,
                thumbnail_height = ?,
                attach_count = 1
            ON DUPLICATE KEY
            UPDATE user_id = ?,
                   upload_date = ?,
                   filename = ?,
                   file_size = ?,
                   file_hash = ?,
                   width = ?,
                   height = ?,
                   thumbnail_width = ?,
                   thumbnail_height = ?,
                   attach_count = 1
        `,
			item.ID, album.OwnerUserID, item.OriginationTimestamp, item.PathComponent, imageFileSize, mediaFileHash, originalImageWidth, originalImageHeight, newThumbnailWidth, newThumbnailHeight,
			album.OwnerUserID, item.OriginationTimestamp, item.PathComponent, imageFileSize, mediaFileHash, originalImageWidth, originalImageHeight, newThumbnailWidth, newThumbnailHeight,
		)
		if err != nil {
			return AddErrorInfo(err, "insert into xf_attachment_data")
		}

		counter++
	}

	return err
}
