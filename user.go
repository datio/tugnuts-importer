// Copyright 2018 tugnuts. All rights reserved.

package phpbbxf2

import (
	"database/sql"
	"errors"
)

var galleryUserIDToUsernameMap = map[int]string{}
var galleryUserIDToForumID = map[int]int{}

func (o *Import) prepareUserDataFromGalleryTable() error {
	var (
		userRows      *sql.Rows
		galleryUserID int
		username      string
		forumUserIDA  int
		forumUserIDB  int
	)

	userRows, err = phpbb.Txn.Query(`
		SELECT g2_User.g_id,
		       g2_User.g_fullName,
			   COALESCE(a.user_id, 0),
			   COALESCE(b.user_id, 0)
		FROM g2_User
		LEFT JOIN phpbb_users AS a ON g2_User.g_fullName = a.username_clean
		LEFT JOIN phpbb_users AS b ON g2_User.g_fullName = b.username
	`)

	for userRows.Next() {
		err = userRows.Scan(&galleryUserID, &username, &forumUserIDA, &forumUserIDB)
		if err != nil {
			return err
		}

		forumUserID := forumUserIDA
		if forumUserID == 0 {
			forumUserID = forumUserIDB
		}

		galleryUserIDToUsernameMap[galleryUserID] = username
		galleryUserIDToForumID[galleryUserID] = forumUserID
	}

	return err
}

const mailOkChoices = `{"1":"Yes","2":"No"}`

const rangerModelChoices = `{"9":"R-29 CB","10":"R-31 S","2":"R-21 EC","3":"R-23","4":"R-25 Classic","5":"R-25 SC","6":"R-27","7":"R-29 Classic","8":"R-29 S","1":"R-21","11":"R-31 CB","12":"C-24","13":"C-242 C","14":"C-242 SC","15":"C-26","16":"C-28","17":"C-30 S","18":"C-30 CB","19":"C-30 ST","20":"C-302 C","21":"C-302 SC"}`

const stateFieldChoices = `{"73":"Other","72":"YT","71":"WY","70":"WV","69":"WI","68":"WA","67":"VT","66":"VI","65":"VA","64":"UT","63":"TX","62":"TN","61":"SK","60":"SD","59":"SC","58":"RI","57":"QC","56":"PW","55":"PR","54":"PE","53":"PA","52":"OR","51":"ON","50":"OK","49":"OH","48":"NY","47":"NV","46":"NU","45":"NT","44":"NS","43":"NM","42":"NL","41":"NJ","40":"NH","39":"NE","38":"ND","37":"NC","36":"NB","35":"MT","34":"MS","33":"MP","32":"MO","31":"MN","30":"MI","29":"MH","28":"ME","27":"MD","26":"MB","25":"MA","24":"LA","23":"KY","22":"KS","21":"IN","20":"IL","19":"ID","18":"IA","17":"HI","16":"GU","15":"GA","14":"FM","13":"FL","12":"DE","11":"DC","10":"CT","9":"CO","8":"CA","7":"BC","6":"AZ","5":"AS","4":"AR","3":"AL","2":"AK","1":"AB"}`

func (o *Import) importDropdownUserFieldValues() error {
	mailOkFieldID, ok := o.Config.Get("phpbbxf2.mail_ok_field_id").(string)
	if !ok {
		return errors.New("could not find value of 'phpbbxf2.mail_ok_field_id' in config")
	}

	rangerFieldID, ok := o.Config.Get("phpbbxf2.ranger_model_field_id").(string)
	if !ok {
		return errors.New("could not find value of 'phpbbxf2.ranger_model_field_id' in config")
	}

	stateFieldID, ok := o.Config.Get("phpbbxf2.state_field_id").(string)
	if !ok {
		return errors.New("could not find value of 'phpbbxf2.state_field_id' in config")
	}

	_, err = xf2.Txn.Exec(`
		UPDATE xf_user_field
		SET field_choices = ?
		WHERE field_id = ?
	`, mailOkChoices, mailOkFieldID)
	if err != nil {
		return AddErrorInfo(err, "update mail_ok user field choices")
	}

	_, err = xf2.Txn.Exec(`
		UPDATE xf_user_field
		SET field_choices = ?
		WHERE field_id = ?
	`, rangerModelChoices, rangerFieldID)
	if err != nil {
		return AddErrorInfo(err, "update ranger_model user field choices")
	}

	_, err = xf2.Txn.Exec(`
		UPDATE xf_user_field
		SET field_choices = ?
		WHERE field_id = ?
	`, stateFieldChoices, stateFieldID)
	if err != nil {
		return AddErrorInfo(err, "update state user field choices")
	}

	var (
		userFieldRows *sql.Rows
		userID        int
		mailOk        string
		rangerModel   string
		state         string
	)

	userFieldRows, err = phpbb.Txn.Query(`
		SELECT user_id,
		       COALESCE(pf_mail_ok, '') AS pf_mail_ok,
			   COALESCE(pf_ranger_model, '') AS pf_ranger_model,
			   COALESCE(pf_state, '') AS pf_state
		FROM phpbb_profile_fields_data
	`)

	for userFieldRows.Next() {
		err = userFieldRows.Scan(&userID, &mailOk, &rangerModel, &state)
		if err != nil {
			return err
		}

		_, err = xf2.Txn.Exec(`
			UPDATE xf_user_field_value
			SET field_value = ?
			WHERE field_id = ?
			  AND user_id = ?
		`, mailOk, mailOkFieldID, userID)
		if err != nil {
			return AddErrorInfo(err, "update mail_ok user field value")
		}

		_, err = xf2.Txn.Exec(`
			UPDATE xf_user_field_value
			SET field_value = ? - 1
			WHERE field_id = ?
			  AND user_id = ?
		`, rangerModel, rangerFieldID, userID)
		if err != nil {
			return AddErrorInfo(err, "update ranger_model user field value")
		}

		_, err = xf2.Txn.Exec(`
			UPDATE xf_user_field_value
			SET field_value = ? - 1
			WHERE field_id = ?
			  AND user_id = ?
		`, state, stateFieldID, userID)
		if err != nil {
			return AddErrorInfo(err, "update state user field value")
		}
	}

	return err
}
