// Copyright 2018 tugnuts. All rights reserved.

package phpbbxf2

import (
	"io/ioutil"
	"os"
)

func (o *Import) migrate() error {
	if err = o.createXFMGImportLogTable(); err != nil {
		return err
	}

	err = xf2.Txn.Commit()
	if err != nil {
		return err
	}

	xf2.Txn, err = xf2.Db.Begin()
	if err != nil {
		return err
	}

	err = o.prepareUserDataFromGalleryTable()
	if err != nil {
		return err
	}

	// Import data related to the XFMG add-on.
	if err = o.importMediaGallery(); err != nil {
		return err
	}

	if err = o.importDropdownUserFieldValues(); err != nil {
		return err
	}

	return xf2.Txn.Commit()
}

func (o *Import) createEmptyDirIfNotExists(dirPath string, createEmptyHTLMFile bool) error {
	_, err = os.Stat(dirPath)
	if os.IsNotExist(err) {
		if err = os.MkdirAll(dirPath, 0755); err != nil {
			return err
		}
	} else {
		return nil
	}

	// XenForo index.html files include a space.
	if createEmptyHTLMFile {
		err = ioutil.WriteFile(dirPath+"index.html", []byte(" "), 0644)
	}

	return err
}
